import { useSelector, useDispatch } from 'react-redux'
import { counterSizeChange } from './counterSlice'

const CounterSizeGenerator = () => {
    const countList = useSelector(state => state.counter.countList)
    const dispatch = useDispatch()
    const size = countList.length
    
    const handleValueChage = (e) => {
        dispatch(counterSizeChange(Number(e.target.value)))
    }

    return(
        <div>
            Size: <input type='number' value={size} onChange={handleValueChage}></input>
        </div>
    )
}

export default CounterSizeGenerator