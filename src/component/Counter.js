import { useDispatch } from 'react-redux'
import { increment, decrement } from './counterSlice'

const Counter = ({value,index}) => {
    const dispatch = useDispatch()

    const handleIncrease = () => {
        dispatch(increment(index))
    }

    const handleDecrease = () => {
        dispatch(decrement(index))
    }

    return (
        <div>
            <button onClick={handleIncrease}>+</button>
            <span>{value}</span>
            <button onClick={handleDecrease}>-</button>
        </div>
    )
}

export default Counter