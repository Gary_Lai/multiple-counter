import { useSelector } from "react-redux"

const CounterGroupSum = () => {
    const countList = useSelector(state => state.counter.countList)
    const sum = countList.reduce((counter,number) => counter + number,0)
    return (
        <div>
            Sum: {sum}
        </div>
    )
}

export default CounterGroupSum